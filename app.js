// extraido de Dev Ed en Youtube https://www.youtube.com/watch?v=MHPGeQD8TvI
// leyendo el DOM
const section = document.querySelector("section");
const playerLivesCount = document.querySelector("span");
let playerLives = 6; // esto si va a variar por eso el let

// listar el texto
playerLivesCount.textContent = playerLives;

// Generando la data
const getData = () => [
	{ imgSrc: "assets/imgs/1.jpg", name: "Imagen 1" },
	{ imgSrc: "assets/imgs/2.jpg", name: "Imagen 2" },
	{ imgSrc: "assets/imgs/3.jpg", name: "Imagen 3" },
	{ imgSrc: "assets/imgs/4.jpg", name: "Imagen 4" },
	{ imgSrc: "assets/imgs/5.jpg", name: "Imagen 5" },
	{ imgSrc: "assets/imgs/6.jpg", name: "Imagen 6" },
	{ imgSrc: "assets/imgs/7.jpg", name: "Imagen 7" },
	{ imgSrc: "assets/imgs/8.jpg", name: "Imagen 8" },
	{ imgSrc: "assets/imgs/1.jpg", name: "Imagen 1" },
	{ imgSrc: "assets/imgs/2.jpg", name: "Imagen 2" },
	{ imgSrc: "assets/imgs/3.jpg", name: "Imagen 3" },
	{ imgSrc: "assets/imgs/4.jpg", name: "Imagen 4" },
	{ imgSrc: "assets/imgs/5.jpg", name: "Imagen 5" },
	{ imgSrc: "assets/imgs/6.jpg", name: "Imagen 6" },
	{ imgSrc: "assets/imgs/7.jpg", name: "Imagen 7" },
	{ imgSrc: "assets/imgs/8.jpg", name: "Imagen 8" },
];

// randomize
const randomize = () => {
	const cardData = getData();
	cardData.sort(() => Math.random() - 0.5); // Con esto hago que las imagenes siempre cmbien de lugar
	return cardData;
}

// funcion Generador de Cards
const cardGenerator = () => {
	const cardData = randomize();
	// Generando el HTML
	cardData.forEach(( item ) => {
		const card = document.createElement("div");
		const face = document.createElement("img");
		const back = document.createElement("div");
		// agregando las clases q voy a necesitar a los elementos creados
		card.classList = "card";
		face.classList = "face";
		back.classList = "back";
		// meter la info de las imgs dentro de los cards
		face.src = item.imgSrc;
		card.setAttribute('name', item.name);
		// meter las cards en el section
		section.appendChild(card); // card dentro del section
		card.appendChild(face); // imagen de frente dentro del card
		card.appendChild(back); // background de la imagen para cuando esté volteada la card
		// Con esto hacemos q se voltee la card cuando se haga click
		card.addEventListener("click", (e) => {
			card.classList.toggle("toggleCard");
			checkCards(e);
		});
	});

};

// Chequear Cards
const checkCards = (e) => {
	// console.log(e);
	const clickedCard = e.target;
	clickedCard.classList.add("flipped");
	const flippedCards = document.querySelectorAll(".flipped" );
	const toggleCard = document.querySelectorAll(".toggleCard");
	// logica del juego
	if( flippedCards.length === 2 ) {
		if( flippedCards[0].getAttribute('name') === flippedCards[1].getAttribute('name') ) {
			console.log('match');
			flippedCards.forEach((card) => {
				card.classList.remove("flipped");
				card.style.pointeEvents = "none";
			});
		} else {
			console.log('fallaste');
			flippedCards.forEach((card) => {
				card.classList.remove("flipped");
				setTimeout(() => card.classList.remove("toggleCard"), 1000);
			});
			playerLives--; // reduzco los intentos si no hay match
			playerLivesCount.textContent = playerLives; // muestro en pantalla los intentos que le quedan
			// si llega a cero resetea el juego
			if (playerLives === 0) {
				restart("Prueba Otra Vez");
			}
		}
	}
	// si destapa todas las card entonces ganó
	if( toggleCard.length === 16) {
		restart("Ganaste.! Felicidades.");
	}
};

// Reiniciar el juego cuando se destapan todas
const restart = (text) => {
	let cardData = randomize();
	let faces = document.querySelectorAll(".face");
	let cards = document.querySelectorAll(".card");
	section.style.pointeEvents = "none"; // para deshabilidar el click mientras se resetea
	cardData.forEach((item, index) => {
		cards[index].classList.remove("toggleCard");
		// Randomize otra vez pero dando tiempo a q se resetee
		setTimeout(() => {
			cards[index].style.pointeEvents = "all";
			faces[index].src = item.imgSrc;
			cards[index].setAttribute("name", item.name);
			section.style.pointeEvents = "all";
		}, 1000);
	});
	playerLives = 6;
	playerLivesCount.textContent = playerLives;
	setTimeout(() => window.alert(text), 100)
}

cardGenerator();